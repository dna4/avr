include cpu_info

CC = avr-gcc
CFLAGS = -Wall -pedantic -std=c99 -O2
CFLAGS := $(CFLAGS) -I./ds -I./arch -I./drivers
CFLAGS := $(CFLAGS) -mmcu=$(CPU) -DMCU=$(CPU) -DF_CPU=$(F_CPU)

DRDIR = drivers
ARDIR = arch
DSDIR = ds
APDIR = apps

IRQMODULE := $(addsuffix .c, $(CPU))
IRQMODULE := $(addprefix irq_, $(IRQMODULE))
IRQMODULE := $(ARDIR)/$(IRQMODULE)

DRMODULES = $(wildcard $(DRDIR)/*.c)
DSMODULES = $(wildcard $(DSDIR)/*.c)
APMODULES = $(wildcard $(APDIR)/*.c)

SRCMODULES := $(DRMODULES) $(DSMODULES) $(APMODULES) $(IRQMODULE)
OBJMODULES := $(SRCMODULES:.c=.o)
IRQMODULE := $(IRQMODULE:.c=.o)

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

%.hex: %.elf
	avr-objcopy -j .text -j .data -O ihex $< $@


all: uart.hex rpn_calc.hex uart_echo_test.hex

clean:
	find . -name '*.hex' -delete
	find . -name '*.o' -delete
	find . -name '*.elf' -delete


uart.hex: uart.elf
uart.elf: $(DRDIR)/uart.o $(IRQMODULE) $(DSDIR)/ringbuf.o $(APDIR)/uart_int_test.o
	$(CC) $(CFLAGS) $^ -o $@


rpn_calc.hex: rpn_calc.elf
rpn_calc.elf: $(DRDIR)/uart.o $(IRQMODULE) $(DSDIR)/ringbuf.o \
              $(APDIR)/libio.o $(DSDIR)/istack.o $(APDIR)/rpn_calc.o
	$(CC) $(CFLAGS) $^ -o $@


uart_echo.hex: uart_echo.elf
uart_echo.elf: $(APDIR)/uart_echo.o $(DRDIR)/uart.o $(IRQMODULE) $(DSDIR)/ringbuf.o
	$(CC) $(CFLAGS) $^ -o $@


lcd_test.hex: lcd_test.elf
lcd_test.elf: $(APDIR)/lcd_test.o $(DRDIR)/lcd1602.o
	$(CC) $(CFLAGS) $^ -o $@


timer16.hex: timer16.elf
timer16.elf: $(APDIR)/timer_16.o $(DRDIR)/timer16.o $(IRQMODULE) \
             $(DSDIR)/mpool.o $(DSDIR)/list.o
	$(CC) $(CFLAGS) $^ -o $@


spi_test.hex: spi_test.elf
spi_test.elf: $(DRDIR)/spi.o $(IRQMODULE) $(APDIR)/spi_test.o
	$(CC) $(CFLAGS) $^ -o $@


uart_echo_event_loop.hex: uart_echo_event_loop.elf
uart_echo_event_loop.elf: $(APDIR)/uart_echo_event_loop.o $(DRDIR)/uart.o \
                        $(IRQMODULE) $(DSDIR)/ringbuf.o $(DRDIR)/dispatch.o \
                        $(DSDIR)/mpool.o $(DSDIR)/list.o
	$(CC) $(CFLAGS) $^ -o $@


mpool_test.hex: mpool_test.elf
mpool_test.elf: $(APDIR)/mpool_test.o $(DSDIR)/mpool.o
	$(CC) $(CFLAGS) $^ -o $@


list_test.hex: list_test.elf
list_test.elf: $(APDIR)/list_test.o $(DSDIR)/mpool.o $(DSDIR)/list.o
	$(CC) $(CFLAGS) $^ -o $@


btn_event_loop.hex: btn_event_loop.elf
btn_event_loop.elf: $(APDIR)/btn_event_loop.o $(DRDIR)/btn.o \
                        $(IRQMODULE) $(DRDIR)/dispatch.o $(DRDIR)/timer16.o \
                        $(DSDIR)/mpool.o $(DSDIR)/list.o
	$(CC) $(CFLAGS) $^ -o $@
