#ifndef DISPATCH
#define DISPATCH


#include "list.h"


#define DEV_NODE_COUNT 3
#define DEV_HAVE_NO_EVENTS 0

struct device_node {
    struct list_ctx links;
    void *dev;
    void (*handler)(void *dev, int events);
    int events;
};


void dispatch_init();
void dispatch_start();

struct device_node *
dispatch_add(void *dev, void (*handler)(void *dev, int events), int events);
void dispatch_remove(struct device_node *devnode);

#endif
