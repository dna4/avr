#ifndef BUTTON_DRIVER
#define BUTTON_DRIVER


#include <inttypes.h>
#include <stdlib.h>

#include "timer16.h"


enum {
    BTN_PRESSED = 1,
    BTN_HOLD_DOWN = 2,
    BTN_RELEASED = 4,
};

struct btn_ctx {
    int (*get_status)(void *dev);
    struct timer16_ctx *tmr;
    struct timer_node *hold_btn_node;
    volatile uint8_t *eicr;
    volatile uint8_t *eimsk;
    volatile uint8_t *eifr;
    uint8_t eicr_num;
    uint8_t eimsk_num;
    int state;
    int event;
};


void btn_init(struct btn_ctx *ctx, struct timer16_ctx *tmr,
    volatile uint8_t *eicr, volatile uint8_t *eimsk, volatile uint8_t *eifr,
    uint8_t eicr_num, uint8_t eimsk_num, uint8_t btn_irq
);
int btn_get_event(struct btn_ctx *ctx);

#endif
