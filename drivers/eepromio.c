#include "eepromio.h"

#include <avr/io.h>


static inline void wait_ready()
{
    uint8_t ready = 1 << EEPE;
    while (EECR & ready)
        {}
}

uint8_t eeprom_read(void *addr)
{
    wait_ready();
    EEAR = (uint16_t) addr;
    uint8_t enable = 1 << EERE;
    EECR |= enable;
    return EEDR;
}

void eeprom_write(void *addr, uint8_t data)
{
    wait_ready();
    EEAR = (uint16_t) addr;
    EEDR = data;
    EECR |= (1 << EEMPE);
    EECR |= (1 << EEPE);
}
