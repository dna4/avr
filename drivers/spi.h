#ifndef SPI_DRIVER
#define SPI_DRIVER


#include <inttypes.h>
#include <stdlib.h>


enum {
    SPI_SPEED4 = 0,
    SPI_SPEED16,
    SPI_SPEED64,
    SPI_SPEED128,
};

struct spi_ctx {
    volatile uint8_t *base;
    struct spi_request *req;
    size_t curr;
};

struct spi_request {
    void (*handler_done)(struct spi_ctx *ctx);
    void *txbuf;
    void *rxbuf;
    size_t sz;
};


void spi_master_init(struct spi_ctx *ctx,
    volatile uint8_t *base, uint8_t spi_irq, int speed,
    volatile uint8_t *ddr, uint8_t mosi, uint8_t sck, uint8_t ss
);
void spi_transfer(struct spi_ctx *ctx, struct spi_request *req);
void spi_abort(struct spi_ctx *ctx);
int spi_isbusy(struct spi_ctx *ctx);

#endif
