#ifndef TIMER8_DRIVER
#define TIMER8_DRIVER


#include <inttypes.h>
#include <stdlib.h>


enum {
    TIMER_NORMAL = 1,
    TIMER_PRSCLR_8 = 2,
    TIMER_PRSCLR_64 = 3,
    TIMER_PRSCLR_256 = 4,
    TIMER_PRSCLR_1024 = 5,
};

struct timer8_ctx {
    volatile uint8_t *tccra;
    volatile uint8_t *tccrb;
    volatile uint8_t *tifr;
    volatile uint8_t *tcnt;
    volatile uint8_t *ocra;
    volatile uint8_t *timsk;
};

void timer8_init(struct timer8_ctx *ctx,
    volatile uint8_t *tccra,
    volatile uint8_t *tccrb,
    volatile uint8_t *tifr,
    volatile uint8_t *tcnt,
    volatile uint8_t *ocra,
    volatile uint8_t *timsk
);
void timer8_start(struct timer8_ctx *ctx,
    uint8_t cmp_irq,
    uint8_t tick_count,
    int mode,
    void (*handler)(void *),
    void *arg
);
void timer8_disable(struct timer8_ctx *ctx);

#endif
