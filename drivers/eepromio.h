#include <inttypes.h>

uint8_t eeprom_read(void *addr);
void eeprom_write(void *addr, uint8_t data);
