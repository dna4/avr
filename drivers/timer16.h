#ifndef TIMER8_DRIVER
#define TIMER8_DRIVER


#include <inttypes.h>
#include <stdlib.h>

#include "mpool.h"
#include "list.h"


enum {
    TIMER16_NORMAL = 1,
    TIMER16_PRSCLR_8 = 2,
    TIMER16_PRSCLR_64 = 3,
    TIMER16_PRSCLR_256 = 4,
    TIMER16_PRSCLR_1024 = 5,
};

enum {
    TIMER16_CONTINUE = 0,
    TIMER16_STOP,
};

struct timer_node {
    struct list_ctx links;
    int (*handler)(void *ctx);
    void *ctx;
    uint16_t ticks;
    uint16_t now_ticks;
};

struct handler_ctx {
    int (*handler)(void *ctx);
    void *ctx;
    uint16_t ticks;
    struct timer_node *pnode;
};

struct timer_regs {
    volatile uint8_t *tccra;
    volatile uint8_t *tccrb;
    volatile uint8_t *tifr;
    volatile uint8_t *tcnt;
    volatile uint8_t *ocra;
    volatile uint8_t *timsk;
    int mode;
};

struct timer16_ctx {
    /* list of registered handlers */
    struct list_ctx head;
    /* memory pool for list nodes */
    struct mpool_ctx mpool;
    struct timer_regs regs;
};

void timer16_init(struct timer16_ctx *ctx,
    void *buf, size_t sz,
    uint8_t cmp_irq, int mode,
    volatile uint8_t *tccra,
    volatile uint8_t *tccrb,
    volatile uint8_t *tifr,
    volatile uint8_t *tcnt,
    volatile uint8_t *ocra,
    volatile uint8_t *timsk
);
void timer16_disable(struct timer16_ctx *ctx);

int timer16_add_handlers(
    struct timer16_ctx *ctx, struct handler_ctx *handlers, size_t count
);
void timer16_delete(struct timer16_ctx *ctx, struct timer_node *pnode);


#endif
