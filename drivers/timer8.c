#include "timer8.h"
#include "irq.h"

#include <avr/io.h>
#include <avr/interrupt.h>


void timer8_init(struct timer8_ctx *ctx,
    volatile uint8_t *tccra,
    volatile uint8_t *tccrb,
    volatile uint8_t *tifr,
    volatile uint8_t *tcnt,
    volatile uint8_t *ocra,
    volatile uint8_t *timsk
)
{
    ctx->tccra = tccra;
    ctx->tccrb = tccrb;
    ctx->tifr = tifr;
    ctx->tcnt = tcnt;
    ctx->ocra = ocra;
    ctx->timsk = timsk;

    timer8_disable(ctx);
}

void timer8_start(struct timer8_ctx *ctx,
    uint8_t cmp_irq,
    uint8_t tick_count,
    int mode,
    void (*handler)(void *),
    void *arg
)
{
    irq_request(cmp_irq, handler, arg);

    *ctx->tcnt = 0;
    *ctx->ocra = tick_count;
    /* enable interrupt on compare */
    *ctx->timsk = 1 << OCIE0A;
    /* set CTC mode */
    *ctx->tccra = (1 << WGM01);
    /* start timer */
    *ctx->tccrb = mode;
}

void timer8_disable(struct timer8_ctx *ctx)
{
    /* disable timer */
    *ctx->tccrb = 0;
    /* disable all interrupts */
    *ctx->timsk = 0;
    /* clear all flags */
    *ctx->tifr = (1 << OCF0B) | (1 << OCF0A) | (1 << TOV0);
}
