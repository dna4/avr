#ifndef UART_DRIVER
#define UART_DRIVER


#include "ringbuf.h"
#include <inttypes.h>
#include <stdlib.h>

#define UART_DISPATCH_MODE

#ifdef UART_DISPATCH_MODE
enum {
    UART_HAVE_RX_DATA = 1,
    UART_HAVE_TX_DATA = 2,
    UART_TX_BUF_EMPTY = 4,
};
#endif


/*
changed params:
    - baud rate

unchanged params:
    - 1 stop bit +-
    - no parity +-
    - using interrupts +-
    - rx/tx enable +-
    - 8-bit data
*/

struct uart_irqs {
    uint8_t rx;
    uint8_t empty;
};

struct uart_errs {
/* hardware data overrun count */
    uint32_t hwdor;
/* software data overrun count*/
    uint32_t swdor;
/* frame error count */
    uint32_t fe;
};

struct uart_ctx {
#ifdef UART_DISPATCH_MODE
    int (*get_status)(void *dev);
#endif
    volatile uint8_t *base;
    struct uart_irqs irqs;
    struct uart_errs errs;
    struct rbuf_struct rxbuf;
    struct rbuf_struct txbuf;
};

void uart_init(struct uart_ctx *ctx,
    void *rxbuf, size_t rxsz,
    void *txbuf, size_t txsz,
    uint8_t rx, uint8_t empty,
    uint32_t baudrate, volatile uint8_t *base
);
size_t uart_write(struct uart_ctx *ctx, void *buf, size_t sz);
size_t uart_read(struct uart_ctx *ctx, void *buf, size_t sz);
int uart_have_errs(struct uart_ctx *ctx);

int uart_test_reg_addrs(struct uart_ctx *ctx,
    volatile uint8_t *ucsra, volatile uint8_t *ucsrb,
    volatile uint8_t *ucsrc, volatile uint8_t *ubrrl,
    volatile uint8_t *ubrrh, volatile uint8_t *udr
);

#endif
