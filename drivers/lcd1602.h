#ifndef LCD1602_DRIVER
#define LCD1602_DRIVER


/*
    Driver for lcd1602 display.
    It is implemented only restricted  drawing functions.
*/

#include <inttypes.h>

struct lcd_ctx {
    volatile uint8_t *data;
    volatile uint8_t *ddir;
    volatile uint8_t *ctrl; // pins: 1 - RS, 2 - RW, 3 - E
};


void lcd_init(struct lcd_ctx *ctx,
    volatile uint8_t *data,
    volatile uint8_t *ddir,
    volatile uint8_t *ctrl,
    volatile uint8_t *ctrldir
);
void lcd_print(struct lcd_ctx *ctx, char *str);
void lcd_clear(struct lcd_ctx *ctx);


#endif
