#include <avr/io.h>
#include <util/delay.h>

#include "uart.h"
#include "irq.h"

#ifdef UART_DISPATCH_MODE

#include "dispatch.h"

#endif


enum {
    UCSRA_OFFSET = 0,
    UCSRB_OFFSET = 1,
    UCSRC_OFFSET = 2,
    UBRRL_OFFSET = 4,
    UBRRH_OFFSET = 5,
    UDR_OFFSET = 6
};

/* Turn on tx, rx transfer, enable rx interrupt */
#define UCSRB_FLAGS ((1 << RXEN0) | (1 << TXEN0) | (1 << RXCIE0))

#define EMPTY_INT (1 << UDRIE0)

/* 8-bit frame, 1-stopbit, no parity bits, async mode */
#define UCSRC_FLAGS ((1 << UCSZ00) | (1 << UCSZ01))


static inline uint16_t get_desired_baudrate(uint32_t baud)
{
    return ((F_CPU / (baud * 16UL))) - 1;
}

static inline volatile uint8_t *
get_reg_addr(volatile uint8_t *base, size_t offset)
{
    return base + offset;
}

static void handler_rx(void *arg)
{
    struct uart_ctx *ctx = (struct uart_ctx *) arg;
    struct uart_errs *errs = &ctx->errs;

    volatile uint8_t *ucsra = get_reg_addr(ctx->base, UCSRA_OFFSET);
    volatile uint8_t *udr = get_reg_addr(ctx->base, UDR_OFFSET);

    uint8_t status = *ucsra;
    uint8_t data = *udr;
    
    if (status & (1 << DOR0))
        errs->hwdor++;

    if (status & (1 << FE0)) {
        errs->fe++;
        return; /* don't save corrupted data*/
    }

    if (rbuf_push(&ctx->rxbuf, data))
        errs->swdor++;
}

static void handler_empty(void *arg)
{
    struct uart_ctx *ctx = (struct uart_ctx *) arg;
    uint8_t data;
    size_t count = rbuf_read(&ctx->txbuf, &data, sizeof(data));
    if (count == 0) {
    /*all data was transmitted, disable interrupt on empty UDR*/
        volatile uint8_t *ucsrb = get_reg_addr(ctx->base, UCSRB_OFFSET);
        *ucsrb &= ~EMPTY_INT;
        return;
    }
    volatile uint8_t *udr = get_reg_addr(ctx->base, UDR_OFFSET);
    *udr = data;
}

#ifdef UART_DISPATCH_MODE

static int uart_get_status(void *dev)
{
    struct uart_ctx *ctx = (struct uart_ctx *) dev;

    int status = DEV_HAVE_NO_EVENTS;

    size_t filled_size = rbuf_filled_size(&ctx->rxbuf);
    if (filled_size > 0)
        status |= UART_HAVE_RX_DATA;

    size_t free_size = rbuf_free_size(&ctx->txbuf);
    if (free_size > 0)
        status |= UART_HAVE_TX_DATA;

    filled_size = rbuf_filled_size(&ctx->txbuf);
    if (filled_size == 0)
        status |= UART_TX_BUF_EMPTY;
    
    return status;
}

#endif


void uart_init(struct uart_ctx *ctx,
    void *rxbuf, size_t rxsz,
    void *txbuf, size_t txsz,
    uint8_t rx, uint8_t empty,
    uint32_t baudrate, volatile uint8_t *base
)
{
    struct uart_irqs *irqs = &ctx->irqs;
    irqs->rx = rx;
    irqs->empty = empty;
    irq_request(rx, handler_rx, ctx);
    irq_request(empty, handler_empty, ctx);

    rbuf_init(&ctx->rxbuf, rxbuf, rxsz);
    rbuf_init(&ctx->txbuf, txbuf, txsz);

    struct uart_errs *errs = &ctx->errs;
    errs->swdor = errs->hwdor = errs->fe = 0;

    ctx->base = base;
    volatile uint8_t *ubbrh = get_reg_addr(base, UBRRH_OFFSET);
    volatile uint8_t *ubbrl = get_reg_addr(base, UBRRL_OFFSET);

    uint16_t baud = get_desired_baudrate(baudrate);
    *ubbrh = baud >> 8;
    *ubbrl = baud;

    volatile uint8_t *ucsrc = get_reg_addr(base, UCSRC_OFFSET);
    volatile uint8_t *ucsrb = get_reg_addr(base, UCSRB_OFFSET);

    *ucsrc = UCSRC_FLAGS;
    *ucsrb = UCSRB_FLAGS;

#ifdef UART_DISPATCH_MODE
    ctx->get_status = uart_get_status;
#endif
}

size_t uart_write(struct uart_ctx *ctx, void *buf, size_t sz)
{
    size_t count = rbuf_write(&ctx->txbuf, buf, sz);
    if (count == 0)
        goto exit;

    volatile uint8_t *ucsrb = get_reg_addr(ctx->base, UCSRB_OFFSET);
    *ucsrb |= EMPTY_INT;
exit:
    return count;
}

size_t uart_read(struct uart_ctx *ctx, void *buf, size_t sz)
{
    return rbuf_read(&ctx->rxbuf, buf, sz);
}

int uart_have_errs(struct uart_ctx *ctx)
{
    struct uart_errs *errs = &ctx->errs;
    return (errs->fe > 0) || (errs->hwdor > 0) || (errs->swdor > 0);
}

int uart_test_reg_addrs(struct uart_ctx *ctx,
    volatile uint8_t *ucsra, volatile uint8_t *ucsrb,
    volatile uint8_t *ucsrc, volatile uint8_t *ubrrl,
    volatile uint8_t *ubrrh, volatile uint8_t *udr
)
{
    volatile uint8_t *reg = get_reg_addr(ctx->base, UCSRA_OFFSET);
    if (reg != ucsra)
        return 1;
    
    reg = get_reg_addr(ctx->base, UCSRB_OFFSET);
    if (reg != ucsrb)
        return 1;

    reg = get_reg_addr(ctx->base, UCSRC_OFFSET);
    if (reg != ucsrc)
        return 1;

    reg = get_reg_addr(ctx->base, UBRRL_OFFSET);
    if (reg != ubrrl)
        return 1;

    reg = get_reg_addr(ctx->base, UBRRH_OFFSET);
    if (reg != ubrrh)
        return 1;

    reg = get_reg_addr(ctx->base, UDR_OFFSET);
    if (reg != udr)
        return 1;

    return 0;
}
