#include "lcd1602.h"
#include <util/delay.h>
#include <avr/cpufunc.h>


enum {
    OP_WR = 0,
    OP_RD,
};

enum {
    INST = 0,
    DATA,
};

enum {
    LCD_RS = 0,
    LCD_RW,
    LCD_EN,
};

enum {
    CMD_CLR = 0x1,
    CMD_INIT = 0x38, // 1-line, 5x8 cell, 8-bit bus 
    CMD_MODE = 0xF, // display on, cursor on, blink on 
};

#define BUSY_FLAG 7

static void sendcmd(struct lcd_ctx *ctx, int type, uint8_t cmd);
static void wait(struct lcd_ctx *ctx);
static void rise_fall_enb(struct lcd_ctx *ctx);
static int isbusy(uint8_t stat);


void lcd_init(struct lcd_ctx *ctx,
    volatile uint8_t *data,
    volatile uint8_t *ddir,
    volatile uint8_t *ctrl,
    volatile uint8_t *ctrldir
)
{
    *ddir = 0xFF;
    *ctrldir = 0xFF;

    ctx->data = data;
    ctx->ddir = ddir;
    ctx->ctrl = ctrl;

    _delay_ms(2000);

    sendcmd(ctx, INST, CMD_INIT);
    sendcmd(ctx, INST, CMD_MODE);
    //sendcmd(ctx, INST, CMD_CLR);
}

void sendcmd(struct lcd_ctx *ctx, int type, uint8_t cmd)
{
    //wait(ctx);
    *ctx->data = cmd;
    *ctx->ctrl = (type << LCD_RS);
    rise_fall_enb(ctx);
    _delay_us(40);
}

void rise_fall_enb(struct lcd_ctx *ctx)
{
    *ctx->ctrl |= (1 << LCD_EN);
    _NOP();
    _NOP();
    _NOP();
    *ctx->ctrl &= ~(1 << LCD_EN);
}

int isbusy(uint8_t stat)
{
    return stat & (1 << BUSY_FLAG);
}

void wait(struct lcd_ctx *ctx)
{
    *ctx->ddir = 0;
    *ctx->ctrl = (1 << LCD_RW);
    for (;;) {
        rise_fall_enb(ctx);
        uint8_t stat = *ctx->data;
        if (!isbusy(stat))
            break;
    }
    *ctx->ddir = 0xFF;
}

void lcd_print(struct lcd_ctx *ctx, char *str)
{
    for (char ch; (ch = *str) != '\0'; str++)
        sendcmd(ctx, DATA, ch);
}

void lcd_clear(struct lcd_ctx *ctx)
{
    sendcmd(ctx, INST, CMD_CLR);
}
