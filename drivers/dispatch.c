#include <avr/interrupt.h>
#include <stdlib.h>

#include "dispatch.h"
#include "mpool.h"
#include "irq.h"


struct event_info {
    struct device_node *devnode;
    int events;
};

struct device_interface {
    int (*get_status)(void *dev);
};

static struct list_ctx head;
static struct list_ctx *start_node = &head;
static struct device_node memory[DEV_NODE_COUNT];
static struct mpool_ctx mpool;


static void wait_events(struct event_info *pinfo);
static int dev_have_events(
    struct device_node *devnode, struct event_info *pinfo
);


void dispatch_init()
{
    list_head_init(&head);
    mpool_init(&mpool, memory, sizeof(memory), sizeof(*memory));
}

struct device_node *
dispatch_add(void *dev, void (*handler)(void *dev, int events), int events)
{
    if (!handler || !dev || events == DEV_HAVE_NO_EVENTS)
        return NULL;

    struct device_node *devnode = (struct device_node *) mpool_alloc(&mpool);
    if (!devnode)
        return NULL;

    devnode->dev = dev;
    devnode->handler = handler;
    devnode->events = events;

    list_add_to_tail(start_node, devnode);

    return devnode;
}

void dispatch_start()
{
    /* enable global interrupts and start of events dispatching */
    sei();

    for (struct event_info info;;) {
        info.devnode = NULL;
        info.events = 0;

        wait_events(&info);

        struct device_node *devnode = info.devnode;
        devnode->handler(devnode->dev, info.events);
    }
}

void wait_events(struct event_info *pinfo)
{
    for (;;) {
        irq_reset_sleep_flag();

        int have_events = 0;

        /* traversal of circulat list */
        struct list_ctx *p = start_node;
        do {
            if (p == &head) {
                goto skip_list_head;
            }

            struct device_node *devnode = (struct device_node *) p;
            have_events = dev_have_events(devnode, pinfo);
            if (have_events)
                break;

skip_list_head:
            p = p->next;
        } while (p != start_node);

        if (have_events) {
        /* some device have events so we return control for processing
         * next wait_events call will start from next device
         */
            start_node = p->next;
            break;
        }

        /* no devices have events for processing
         * mcu is switched to power-safe mode until any interrupt will raised
         */
        irq_sleep_mcu();
    }
}

int dev_have_events(struct device_node *devnode, struct event_info *pinfo)
{
    int have_events = DEV_HAVE_NO_EVENTS;

    struct device_interface *iface = (struct device_interface *) devnode->dev;
    int status = iface->get_status(devnode->dev);
    int events = status & devnode->events;
    if (events) {
        pinfo->devnode = devnode;
        pinfo->events = events ;
        have_events = 1;
    }

    return have_events;
}

void dispatch_remove(struct device_node *devnode)
{
    struct list_ctx *pnode = (struct list_ctx *) devnode;
    if (pnode == start_node)
        start_node = start_node->next;
    
    list_delete(pnode);
    mpool_free(&mpool, pnode);
}
