#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>

#include "timer16.h"
#include "irq.h"


static void write_to_reg16(volatile uint8_t *regl, uint16_t val);
static uint16_t read_from_reg16(volatile uint8_t *regl);

static void handler_int(void *arg);

static void free_mem(
    struct mpool_ctx *mpool, struct handler_ctx *handlers, size_t i
);

static void timer16_start(struct timer_regs *regs, uint16_t ticks);
static void timer16_update(
    struct timer16_ctx *ctx, struct handler_ctx *handlers, size_t count
);


void write_to_reg16(volatile uint8_t *regl, uint16_t val)
{
    volatile uint8_t *regh = regl + 1;
    uint8_t vall = val & 0xFF;
    uint8_t valh = val >> 8;

    *regh = valh;
    *regl = vall;
}

uint16_t read_from_reg16(volatile uint8_t *regl)
{
    uint16_t val = 0;
    volatile uint8_t *regh = regl + 1;

    val = *regl;
    val |= (*regh) << 8;

    return val;
}

void handler_int(void *arg)
{
    struct timer16_ctx *ctx = (struct timer16_ctx *) arg;
    struct timer_regs *regs = &ctx->regs;

    timer16_disable(ctx);
    uint16_t past_ticks = read_from_reg16(regs->ocra);
    uint16_t near_next_ticks = UINT16_MAX;

    struct list_ctx *phead = &ctx->head;
    struct list_ctx *pnext;
    for (struct list_ctx *p = phead->next; p != phead; p = pnext) {
        struct timer_node *pnode = (struct timer_node *) p;
        pnext = p->next;

        pnode->now_ticks += past_ticks;
        uint16_t remain_ticks = pnode->ticks - pnode->now_ticks;
        if (remain_ticks > 0) {
            goto skip_exec_handler;
        }

        int ret = pnode->handler(pnode->ctx);
        if (ret == TIMER16_STOP) {
            list_delete(pnode);
            mpool_free(&ctx->mpool, pnode);
            continue;
        }
        /* reset current ticks of handler */
        pnode->now_ticks = 0;
        remain_ticks = pnode->ticks;

skip_exec_handler:
        if (remain_ticks < near_next_ticks)
            near_next_ticks = remain_ticks;
    }

    int is_empty = list_is_empty(phead);
    if (!is_empty)
        timer16_start(regs, near_next_ticks);
}


void timer16_init(struct timer16_ctx *ctx,
    void *buf, size_t sz,
    uint8_t cmp_irq, int mode,
    volatile uint8_t *tccra,
    volatile uint8_t *tccrb,
    volatile uint8_t *tifr,
    volatile uint8_t *tcnt,
    volatile uint8_t *ocra,
    volatile uint8_t *timsk
)
{
    struct timer_regs *regs = &ctx->regs;
    regs->tccra = tccra;
    regs->tccrb = tccrb;
    regs->tifr = tifr;
    regs->tcnt = tcnt;
    regs->ocra = ocra;
    regs->timsk = timsk;
    regs->mode = mode;

    timer16_disable(ctx);

    list_head_init(&ctx->head);
    mpool_init(&ctx->mpool, buf, sz, sizeof(struct timer_node));
    irq_request(cmp_irq, handler_int, ctx);
}

int timer16_add_handlers(
    struct timer16_ctx *ctx, struct handler_ctx *handlers, size_t count
)
{
    for (size_t i = 0; i < count; i++) {
        struct timer_node *pnode =
            (struct timer_node *) mpool_alloc(&ctx->mpool);
        if (!pnode) {
            free_mem(&ctx->mpool, handlers, i);
            return 1;
        }

        struct handler_ctx *h_ctx = &handlers[i];
        h_ctx->pnode = pnode;

        pnode->handler = h_ctx->handler;
        pnode->ctx = h_ctx->ctx;
        pnode->ticks = h_ctx->ticks;
        pnode->now_ticks = 0;
    }

    timer16_update(ctx, handlers, count);

    return 0;
}

void timer16_delete(struct timer16_ctx *ctx, struct timer_node *pnode)
{
    /* prevent access to list from interrupt context */
    uint8_t sreg = SREG;
    cli();

    list_delete(pnode);

    SREG = sreg;

    mpool_free(&ctx->mpool, pnode);
}

void free_mem(struct mpool_ctx *mpool, struct handler_ctx *handlers, size_t i)
{
    for (;;) {
        if (i == 0)
            break;
        i--;
        struct handler_ctx *h_ctx = &handlers[i];
        mpool_free(mpool, h_ctx->pnode);
    }
}

void timer16_update(
    struct timer16_ctx *ctx, struct handler_ctx *handlers, size_t count
)
{
    struct timer_regs *regs = &ctx->regs;

    timer16_disable(ctx);

    /* prevent access to list from interrupt context */
    uint8_t sreg = SREG;
    cli();

    uint16_t cnt_ticks = read_from_reg16(regs->tcnt);

    uint16_t near_next_ticks = UINT16_MAX;
    struct list_ctx *phead = &ctx->head;
    for (struct list_ctx *p = phead->next; p != phead; p = p->next) {
        struct timer_node *pnode = (struct timer_node *) p;
        pnode->now_ticks += cnt_ticks;
        uint16_t next_ticks = pnode->ticks - pnode->now_ticks;
        if (next_ticks < near_next_ticks)
            near_next_ticks = next_ticks;
    }

    for (size_t i = 0; i < count; i++) {
        struct handler_ctx *h_ctx = &handlers[i];
        struct timer_node *pnode = h_ctx->pnode;
        list_add_to_tail(phead, pnode);

        uint16_t next_ticks = pnode->ticks;
        if (next_ticks < near_next_ticks)
            near_next_ticks = next_ticks;
    }

    timer16_start(regs, near_next_ticks);

    SREG = sreg;
}

void timer16_start(struct timer_regs *regs, uint16_t ticks)
{
    /* reset timer counters */
    write_to_reg16(regs->tcnt, 0);
    write_to_reg16(regs->ocra, ticks);

    /* enable interrupt on compare */
    *regs->timsk = 1 << OCIE0A;
    /* set CTC mode and start timer */
    *regs->tccrb = (1 << WGM12) | regs->mode;
}

void timer16_disable(struct timer16_ctx *ctx)
{
    struct timer_regs *regs = &ctx->regs;

    /* disable timer */
    *regs->tccrb = 0;
    /* disable all interrupts */
    *regs->timsk = 0;
    /* clear all flags */
    *regs->tifr = (1 << OCF0B) | (1 << OCF0A) | (1 << TOV0);
}
