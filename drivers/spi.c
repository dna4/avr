#include "spi.h"
#include "irq.h"

#include <avr/io.h>
#include <avr/interrupt.h>


enum {
    SPCR_OFFSET = 0,
    SPSR_OFFSET,
    SPDR_OFFSET,
};


static inline volatile uint8_t *
get_reg_addr(volatile uint8_t *base, size_t offset)
{
    return base + offset;
}

static void handler_transfer(void *arg)
{
    struct spi_ctx *ctx = (struct spi_ctx *) arg;
    struct spi_request *req = ctx->req;
    uint8_t *rxbuf = (uint8_t *) req->rxbuf;
    volatile uint8_t *spdr = get_reg_addr(ctx->base, SPDR_OFFSET);

    rxbuf[ctx->curr] = *spdr;
    ctx->curr++;
    if (ctx->curr < req->sz) {
        /* send next data */
        uint8_t *txbuf = (uint8_t *) req->txbuf;
        *spdr = txbuf[ctx->curr];
        return;
    }

    /* data transfer is completed */
    if (!req->handler_done)
        return;

    req->handler_done(ctx);
}

void spi_master_init(struct spi_ctx *ctx,
    volatile uint8_t *base, uint8_t spi_irq, int speed,
    volatile uint8_t *ddr, uint8_t mosi, uint8_t sck, uint8_t ss
)
{
    ctx->req = NULL;
    ctx->curr = 0;
    ctx->base = base;

    irq_request(spi_irq, handler_transfer, ctx);

    *ddr |= (1 << mosi) | (1 << sck) | (1 << ss);

    volatile uint8_t *spcr = get_reg_addr(base, SPCR_OFFSET);
    int spr0_bit = speed & 1;
    int spr1_bit = speed >> 1;
    *spcr = (1 << SPIE) | (1 << SPE) | (1 << MSTR) |
        (spr1_bit << SPR1) | (spr0_bit << SPR0);
}

void spi_transfer(struct spi_ctx *ctx, struct spi_request *req)
{
    spi_abort(ctx);

    ctx->curr = 0;
    ctx->req = req;
    uint8_t *txbuf = (uint8_t *) req->txbuf;

    volatile uint8_t *spdr = get_reg_addr(ctx->base, SPDR_OFFSET);
    *spdr = *txbuf; /* start data transfer */
}

void spi_abort(struct spi_ctx *ctx)
{
    if (!spi_isbusy(ctx))
        return;

    /* save context */
    uint8_t sreg = SREG;
    /* disable interrupts */
    cli();
    /* safe abort of request */
    ctx->req = NULL;
    ctx->curr = 0;
    /* restore context */
    SREG = sreg; 
}

int spi_isbusy(struct spi_ctx *ctx)
{
    struct spi_request *req = ctx->req;

    return (req) ? (ctx->curr < req->sz) : 0;
}
