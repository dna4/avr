#include "btn.h"
#include "irq.h"


enum {
    LOW_LEVEL_INT = 0,
    ANY_EDGE_INT = 1,
    FAIL_EDGE_INT = 2,
    RISE_EDGE_INT = 3,
};

enum {
    WAIT_PRESS_BTN = 0,
    WAIT_RELEASE_BTN,
};

#define DEBOUNCE_TMR_TICKS 1500
#define HOLDING_BTN_TMR_TICKS 8000
#define HANDLER_COUNT 2


static inline uint8_t get_eicr_flag(int int_type, uint8_t num)
{
    return int_type << (num * 2);
}

static inline void disable_ext_int(struct btn_ctx *ctx)
{
    uint8_t eimsk_disable_int = ~(1 << ctx->eimsk_num);
    *ctx->eimsk &= eimsk_disable_int;
}

static inline void enable_ext_int(struct btn_ctx *ctx)
{
    *ctx->eifr |= (1 << ctx->eimsk_num);
    uint8_t eimsk_enable_int = (1 << ctx->eimsk_num);
    *ctx->eimsk |= eimsk_enable_int;
}

static inline void set_ext_int(struct btn_ctx *ctx, int type)
{
    uint8_t eicr = *ctx->eicr;
    eicr = ~((1 << ctx->eicr_num * 2) + 2);
    uint8_t eicr_flag = get_eicr_flag(type, ctx->eicr_num);
    *ctx->eicr = eicr_flag | eicr;
    *ctx->eifr |= (1 << ctx->eimsk_num);
}

static int debounce_enable_int(void *arg)
{
    struct btn_ctx *ctx = (struct btn_ctx *) arg;
    enable_ext_int(ctx);
    return TIMER16_STOP;
}

static int hold_btn(void *arg)
{
    struct btn_ctx *ctx = (struct btn_ctx *) arg;

    ctx->event = BTN_HOLD_DOWN;

    return TIMER16_CONTINUE;
}

static int btn_get_status(void *dev)
{
    struct btn_ctx *ctx = (struct btn_ctx *) dev;
    return btn_get_event(ctx);
}

static void handler_int(void *arg)
{
    struct btn_ctx *ctx = (struct btn_ctx *) arg;
    
    disable_ext_int(ctx);

    if (ctx->state == WAIT_PRESS_BTN) {
        set_ext_int(ctx, RISE_EDGE_INT);
        ctx->event = BTN_PRESSED;
        ctx->state = WAIT_RELEASE_BTN;
    } else {
        set_ext_int(ctx, FAIL_EDGE_INT);
        ctx->event = BTN_RELEASED;
        ctx->state = WAIT_PRESS_BTN;
        timer16_delete(ctx->tmr, ctx->hold_btn_node);
    }

    struct handler_ctx h_ctx[HANDLER_COUNT];
    
    h_ctx[0].handler = &debounce_enable_int;
    h_ctx[0].ticks = DEBOUNCE_TMR_TICKS;
    h_ctx[0].ctx = ctx;

    h_ctx[1].handler = &hold_btn;
    h_ctx[1].ticks = HOLDING_BTN_TMR_TICKS;
    h_ctx[1].ctx = ctx;
    
    int wait_release_btn = ctx->state == WAIT_RELEASE_BTN;
    size_t handler_count = (wait_release_btn) ? HANDLER_COUNT : 1;
    timer16_add_handlers(ctx->tmr, &h_ctx[0], handler_count);
    if (wait_release_btn) {
        ctx->hold_btn_node = h_ctx[1].pnode;
    }
}

void btn_init(struct btn_ctx *ctx, struct timer16_ctx *tmr,
    volatile uint8_t *eicr, volatile uint8_t *eimsk, volatile uint8_t *eifr,
    uint8_t eicr_num, uint8_t eimsk_num, uint8_t btn_irq
)
{
    ctx->eicr = eicr;
    ctx->eimsk = eimsk;
    ctx->eifr = eifr;
    ctx->eicr_num = eicr_num;
    ctx->eimsk_num = eimsk_num;

    ctx->state = WAIT_PRESS_BTN;
    ctx->tmr = tmr;
    ctx->get_status = &btn_get_status;

    disable_ext_int(ctx);
    set_ext_int(ctx, FAIL_EDGE_INT);
    irq_request(btn_irq, handler_int, ctx);
    enable_ext_int(ctx);
}

int btn_get_event(struct btn_ctx *ctx)
{
    int event = ctx->event;
    ctx->event = 0;
    return event;
}
