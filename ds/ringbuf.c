#include "ringbuf.h"


void rbuf_init(struct rbuf_struct *self, void *buf, size_t sz)
{
    uint8_t *start = (uint8_t *) buf;
    self->start = start;
    self->end = start + sz;
    self->head = self->tail = start;
}

size_t rbuf_size(struct rbuf_struct *self)
{
    return self->end - self->start;
}

size_t rbuf_filled_size(struct rbuf_struct *self)
{
    uint8_t *head = self->head;
    uint8_t *tail = self->tail;
    uint8_t *start = self->start;
    uint8_t *end = self->end;

    return (head <= tail) ?
        (tail - head) :
        (tail - start) + (end - head);
}

size_t rbuf_free_size(struct rbuf_struct *self)
{
    uint8_t *head = self->head;
    uint8_t *tail = self->tail;
    uint8_t *start = self->start;
    uint8_t *end = self->end;

    return (head <= tail) ?
        (end - tail) + (head - start) - 1 :
        (head - tail - 1);
}

static uint8_t *shift(uint8_t *curr, uint8_t *start, uint8_t *end)
{
    curr++;
    return (curr == end) ? start : curr;
}

size_t rbuf_write(struct rbuf_struct *self, void *buf, size_t sz)
{
    size_t free = rbuf_free_size(self);
    size_t written = (sz > free) ? free : sz;
    uint8_t *tail = self->tail;
    uint8_t *buff = (uint8_t *) buf;
    for (size_t i = 0; i < written; i++) {
        *tail = buff[i];
        tail = shift(tail, self->start, self->end);
    }
    self->tail = tail;
    return written;
}

size_t rbuf_read(struct rbuf_struct *self, void *buf, size_t sz)
{
    size_t filled = rbuf_filled_size(self);
    size_t readed = (sz > filled) ? filled : sz;
    uint8_t *head = self->head;
    uint8_t *buff = (uint8_t *) buf;
    for (size_t i = 0; i < readed; i++) {
        buff[i] = *head;
        head = shift(head, self->start, self->end);
    }
    self->head = head;
    return readed;
}

int rbuf_push(struct rbuf_struct *self, uint8_t byte)
{
    uint8_t *tail = self->tail;
    size_t free = rbuf_free_size(self);
    *tail = byte;
    if (free == 0)
        goto overrun;

    self->tail = shift(tail, self->start, self->end);
overrun:
    return (free == 0) ? 1 : 0;
}
