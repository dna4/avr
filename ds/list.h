#ifndef DOUBLE_LIST
#define DOUBLE_LIST


#include <stdlib.h>
#include <inttypes.h>


struct list_ctx {
    void *next;
    void *prev;
};

void list_head_init(struct list_ctx *head);

void list_insert(struct list_ctx *prev,
    struct list_ctx *next, struct list_ctx *node);
void list_add_to_head(struct list_ctx *head, void *node);
void list_add_to_tail(struct list_ctx *head, void *node);

void list_delete(void *pnode);
size_t list_count(struct list_ctx *head);
int list_is_empty(struct list_ctx *head);


#endif
