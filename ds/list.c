#include "list.h"


/* initialize head node of circular list */
void list_head_init(struct list_ctx *head)
{
    head->prev = head->next = head;
}

void list_insert(struct list_ctx *prev,
    struct list_ctx *next, struct list_ctx *node)
{
    next->prev = node;
    node->next = next;
    node->prev = prev;
    prev->next = node;
}

void list_add_to_head(struct list_ctx *head, void *node)
{
    list_insert(head, head->next, (struct list_ctx *) node);
}

void list_add_to_tail(struct list_ctx *head, void *node)
{
    list_insert(head->prev, head, (struct list_ctx *) node);
}

void list_delete(void *pnode)
{
    struct list_ctx *node = (struct list_ctx *) pnode;
    struct list_ctx *prev = node->prev;
    struct list_ctx *next = node->next;

    prev->next = next;
    next->prev = prev;
}

size_t list_count(struct list_ctx *head)
{
    size_t count = 0;
    for (struct list_ctx *p = head->next; p != head; p = p->next)
        count++;
    return count;
}

int list_is_empty(struct list_ctx *head)
{
    return head->next == head;
}
