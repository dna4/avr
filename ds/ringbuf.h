#ifndef RING_BUFFER
#define RING_BUFFER


#include <stdlib.h>
#include <inttypes.h>

struct rbuf_struct {
    uint8_t *start;
    uint8_t *end;
    uint8_t *head;
    uint8_t *tail;
};

void rbuf_init(struct rbuf_struct *self, void *buf, size_t sz);

size_t rbuf_size(struct rbuf_struct *self);
size_t rbuf_filled_size(struct rbuf_struct *self);
size_t rbuf_free_size(struct rbuf_struct *self);

size_t rbuf_write(struct rbuf_struct *self, void *buf, size_t sz);
size_t rbuf_read(struct rbuf_struct *self, void *buf, size_t sz);

int rbuf_push(struct rbuf_struct *self, uint8_t byte);

#endif
