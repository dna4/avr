#ifndef MEMORY_POOL
#define MEMORY_POOL


#include "dispatch.h"

#include <stdlib.h>


struct mpool_ctx {
    void *free_block;
};

void mpool_init(struct mpool_ctx *mpool, void *buf, size_t bufsz, size_t block_sz);

void *mpool_alloc(struct mpool_ctx *mpool);
void mpool_free(struct mpool_ctx *mpool, void *pblock);

size_t mpool_free_count(struct mpool_ctx *mpool);

#endif
