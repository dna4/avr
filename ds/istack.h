#ifndef STACK_INTEGER
#define STACK_INTEGER


#include <stdlib.h>
#include <inttypes.h>


struct istack {
    int32_t *start;
    int32_t *end;
    int32_t *curr;
};

void istack_init(struct istack *self, void *buf, size_t sz);
int istack_push(struct istack *self, int32_t val);
int istack_pop(struct istack *self, int32_t *val);
void istack_clear(struct istack *self);

#endif
