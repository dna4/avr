#include "istack.h"


void istack_init(struct istack *self, void *buf, size_t sz)
{
    int32_t *start = buf;
    self->start = self->curr = start;
    // pointer to stack end is aligned by 4 byte
    size_t count_elem = sz / sizeof(int32_t);
    self->end = start + count_elem;
}

int istack_push(struct istack *self, int32_t val)
{
    if (self->curr == self->end)
        return 1;

    *self->curr = val;
    self->curr++;

    return 0;
}

int istack_pop(struct istack *self, int32_t *val)
{
    if (self->curr == self->start)
        return 1;

    self->curr--;
    *val = *self->curr;

    return 0;
}

void istack_clear(struct istack *self)
{
    self->curr = self->start;
}
