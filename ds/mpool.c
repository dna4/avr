#include <stdlib.h>
#include <inttypes.h>

#include "mpool.h"


struct block {
    struct block *next;
};


static inline struct block *get_block_addr(void *buf,
    size_t index, size_t block_sz
)
{
    uint8_t *base = (uint8_t *) buf;
    return (struct block *) (base + (index * block_sz));
}

/* avr 8-bit mcu don't required aligment of blocks in memory pool */
/* block size must be at least sizeof(void *) == 16-bit for holding pointer to next block */
void mpool_init(struct mpool_ctx *mpool,
    void *buf, size_t bufsz, size_t block_sz
)
{
    size_t count = bufsz / block_sz;
    if ((count == 0) || (block_sz < sizeof(size_t))) {
        mpool->free_block = NULL;
        return;
    }

    mpool->free_block = buf;
    for (size_t i = 0; i < count; i++) {
        struct block *pblock = get_block_addr(buf, i, block_sz);
        struct block *next_block = get_block_addr(buf, i + 1, block_sz);
        pblock->next = (i < count - 1) ? next_block : NULL;
    }
}

void *mpool_alloc(struct mpool_ctx *mpool)
{
    struct block *pblock = (struct block *) mpool->free_block;
    if (!pblock)
        return NULL;

    mpool->free_block = pblock->next;
    return pblock;
}

void mpool_free(struct mpool_ctx *mpool, void *pblock)
{
    if (!pblock)
        return;
    
    struct block *new_free_block = (struct block *) pblock;
    new_free_block->next = mpool->free_block;
    mpool->free_block = new_free_block;
}

size_t mpool_free_count(struct mpool_ctx *mpool)
{
    size_t count = 0;
    struct block *p = (struct block *) mpool->free_block;

    for (; p != NULL; p = p->next)
        count++;

    return count;
}
