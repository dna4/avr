#!/bin/sh

hex=$1

#avrdude -v -patmega328p -carduino -P/dev/ttyACM0 -D -Uflash:w:${hex}
avrdude -v -pm2560 -cstk500 -P/dev/ttyACM0 -D -Uflash:w:${hex}
