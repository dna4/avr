#include <avr/io.h>
#include <inttypes.h>
#include "lcd1602.h"


/*
DDRF = 0xFF;
DDRK = 0xFF;
PORTF = 0xFF;
PORTK = 0xFF;
*/

int main()
{
    struct lcd_ctx lcd;

    lcd_init(&lcd, &PORTK, &DDRK, &PORTF, &DDRF);
    lcd_print(&lcd, "Hello World!!!");

    while (1)
        {}
    
    return 0;
}
