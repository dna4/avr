#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "irq.h"
#include "timer8.h"

struct timer8_ctx ctx;

void led_blink(void *arg)
{
    static uint32_t count = 0;

    if (count < 1000) {
        count++;
        return;
    }

    count = 0;
    /* Toggle a pins on timer overflow */
    PORTK ^= 0xFF;
}

int main()
{
    DDRK = 0xff;
    sei();

    timer8_init(&ctx, &TCCR0A, &TCCR0B, &TIFR0, &TCNT0, &OCR0A, &TIMSK0);
    timer8_start(&ctx, TIMER8_CMPA0, 32, TIMER_PRSCLR_64, &led_blink, 0);

    _delay_ms(20000);
    timer8_disable(&ctx);
    while(1)
        {}

    return 0;
}
