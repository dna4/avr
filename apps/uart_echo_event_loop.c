#include <avr/io.h>
#include <util/delay.h>
#include <inttypes.h>

#include "dispatch.h"
#include "uart.h"
#include "irq.h"


#define BUFSZ 64


struct uart_ctx uart0;
struct device_node *devnode; 

static void uart0_init()
{
    static uint8_t txbuf[BUFSZ];
    static uint8_t rxbuf[BUFSZ];

    uart_init(&uart0, &rxbuf[0], sizeof(rxbuf), &txbuf[0], sizeof(txbuf),
        USART_RX, USART_EMPTY, 9600, &UCSR0A);
}

static void uart_handler(void *dev, int events)
{
    static uint8_t recvbuf[BUFSZ - 1];

    if (events == UART_HAVE_RX_DATA) {
        struct uart_ctx *uart = (struct uart_ctx *) dev;
        size_t count = uart_read(uart, &recvbuf[0], sizeof(recvbuf));
        uart_write(uart, &recvbuf[0], count);
        devnode->events = UART_TX_BUF_EMPTY;
    } else if (events == UART_TX_BUF_EMPTY) {
        devnode->events = UART_HAVE_RX_DATA;
    }
}

int main()
{
    uart0_init();
    dispatch_init();

    devnode = dispatch_add(&uart0, uart_handler, UART_HAVE_RX_DATA);

    dispatch_start();

    return 0;
}
