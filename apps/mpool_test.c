#include <avr/io.h>
#include <util/delay.h>
#include <inttypes.h>

#include "mpool.h"


struct user_struct {
    void *addr;
    uint8_t index;
};

void error()
{
    while (1) {
        PORTK = 2;
        _delay_ms(1000);
        PORTK = 0;
        _delay_ms(1000);
    }
}

void success()
{
    while (1) {
        PORTK = 4;
        _delay_ms(1000);
        PORTK = 0;
        _delay_ms(1000);
    }
}

#define ELEM_COUNT 150

int main()
{
    static struct user_struct memory[ELEM_COUNT];
    static struct user_struct *array[ELEM_COUNT];
    struct mpool_ctx mpool;

    DDRK = 0xff;

    // start test
    PORTK = 1;
    _delay_ms(500);

    mpool_init(&mpool, &memory[0], sizeof(memory), sizeof(*memory));
    size_t count = mpool_free_count(&mpool);
    if (count != ELEM_COUNT)
        error();

    for (size_t i = 0; i < ELEM_COUNT; i++) {
        struct user_struct *p = (struct user_struct *) mpool_alloc(&mpool);
        if (!p)
            error();

        array[i] = p;
        p->addr = p;
        p->index = i;
    }

    count = mpool_free_count(&mpool);
    if (count != 0)
        error();

    for (size_t i = 0; i < ELEM_COUNT; i++) {
        struct user_struct *p = array[i];
        if ((p->addr != p) || (p->index != i))
            error();
    }

    for (size_t i = 0; i < ELEM_COUNT; i++) {
        struct user_struct *p = array[i];
        mpool_free(&mpool, p);
        count = mpool_free_count(&mpool);
        if (count != i + 1)
            error();
    }

    success();

    return 0;
}
