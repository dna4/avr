#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "irq.h"
#include "timer16.h"


#define TMR_NODE_COUNT 3


#include <util/delay.h>
static void error()
{
    while (1) {
        PORTK = 2;
        _delay_ms(1000);
        PORTK = 0;
        _delay_ms(1000);
    }
}

int led_blink1(void *ctx)
{
    static size_t count = 0;

    uint8_t bit_num = 0;
    PORTK ^= (1 << bit_num);

    count++;
    return (count < 5) ? TIMER16_CONTINUE : TIMER16_STOP;
}

int led_blink2(void *ctx)
{
    static size_t count = 0;

    uint8_t bit_num = 1;
    PORTK ^= (1 << bit_num);

    count++;
    return (count < 14) ? TIMER16_CONTINUE : TIMER16_STOP;
}

int led_blink3(void *ctx)
{
    static size_t count = 0;

    uint8_t bit_num = 2;
    PORTK ^= (1 << bit_num);

    count++;
    return (count < 31) ? TIMER16_CONTINUE : TIMER16_STOP;
}

void init_timer()
{
    static struct timer16_ctx ctx;
    static struct timer_node pool_memory[TMR_NODE_COUNT];

    timer16_init(&ctx, pool_memory, sizeof(pool_memory),
        TIMER16_CMPA0, TIMER16_PRSCLR_1024,
        &TCCR1A, &TCCR1B, &TIFR1, &TCNT1L, &OCR1AL, &TIMSK1
    );

    struct handler_ctx h_ctx[TMR_NODE_COUNT];

    uint16_t period = 0xFFFF;
    h_ctx[0].handler = &led_blink1;
    h_ctx[0].ticks = period;
    h_ctx[0].ctx = NULL;

    h_ctx[1].handler = &led_blink2;
    h_ctx[1].ticks = period / 2;
    h_ctx[1].ctx = NULL;

    h_ctx[2].handler = &led_blink3;
    h_ctx[2].ticks = period / 4;
    h_ctx[2].ctx = NULL;

    timer16_add_handlers(&ctx, &h_ctx[0], TMR_NODE_COUNT);
}

int main()
{
    DDRK = 0xff;

    init_timer();

    sei();
    while(1)
        {}

    return 0;
}
