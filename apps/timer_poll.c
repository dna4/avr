#include <avr/io.h>
#include <util/delay.h>
#include <inttypes.h>

void timer_init()
{
    TCCR0B = 0; // stop timer
    TIFR0 = 1; // clear overflow bit
    TCNT0 = 0xfd; // set timer count
    TCCR0B = 7; // start timer, extern event
}

int timer_check()
{
    return (TIFR0 & 1) ? 0 : 1;
}

void toggle()
{
    if (PORTD & 1)
        PORTD -= 1;
    else
        PORTD += 1;
}

int main()
{
    DDRD = 0b00000111;
    while(1) {
        toggle();
        timer_init();
        while (timer_check() != 0) {
            PORTD += 4;
            _delay_ms(1000);
            PORTD -= 4;
            _delay_ms(1000);
        }
    }
    return 0;
}
