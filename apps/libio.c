#include <avr/io.h>
#include <avr/interrupt.h>
#include <inttypes.h>
#include <ctype.h>

#include "uart.h"
#include "irq.h"


#define BUFSZ 64

enum {
    SPACE_CHAR = 0,
    ADD_CHAR,
    RECV_STR,
    ERROR_LONG_STR,
};

static struct uart_ctx uart0;

static void uart0_init()
{
    static uint8_t txbuf[BUFSZ];
    static uint8_t rxbuf[BUFSZ];

    uart_init(&uart0, &rxbuf[0], sizeof(rxbuf), &txbuf[0], sizeof(txbuf),
        USART_RX, USART_EMPTY, 9600, &UCSR0A);
}


static int get_state(int state, char ch, size_t i, size_t bufsz)
{
    if (state == SPACE_CHAR) {
        return (isspace(ch)) ? SPACE_CHAR : ADD_CHAR;
    } else if (state == ADD_CHAR) {
        if (isspace(ch))
            return RECV_STR;

        if (i < bufsz)
            return ADD_CHAR;
    }

    return ERROR_LONG_STR;
}

void print(char *str)
{
    for (size_t sz = sizeof(*str); *str; str++) {
        while (uart_write(&uart0, str, sz) != sz)
            {}
    }
}

char get_char()
{
    uint8_t data;
    size_t sz = sizeof(data);

    while (uart_read(&uart0, &data, sz) != sz)
        {}

    return (char) data;
}

void skip_string()
{
    for (;;) {
        char ch = get_char();
        if (isspace(ch))
            break;
    }
}

int get_string(char *str, size_t sz)
{
    int state = SPACE_CHAR;
    size_t i = 0;
    int ret = 0;

    for (;;) {
        char ch = get_char();

        state = get_state(state, ch, i, sz - 1);
        if (state == SPACE_CHAR) {
            continue;
        } else if (state == ADD_CHAR) {
            str[i] = ch;
            i++;
        } else if (state == RECV_STR) {
            str[i] = '\0';
            break;
        } else {
            str[i] = ch;
            ret = 1;
            break;
        }
    }

    return ret;
}

void io_init()
{
    uart0_init();
}
