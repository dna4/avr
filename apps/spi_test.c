#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <inttypes.h>
#include <string.h>

#include "spi.h"
#include "irq.h"


#define BUFSZ 64

void error()
{
    while (1) {
        PORTK = 2;
        _delay_ms(1000);
        PORTK = 0;
        _delay_ms(1000);
    }
}

void success()
{
    while (1) {
        PORTK = 4;
        _delay_ms(1000);
        PORTK = 0;
        _delay_ms(1000);
    }
}

void fill_buf(void *pbuf, size_t sz)
{
    static uint8_t num = 0;

    uint8_t *buf = (uint8_t *) pbuf;
    for (size_t i = 0; i < sz; i++, num++)
        buf[i] = num;
}

size_t call_count = 0;
void handler_done(struct spi_ctx *ctx)
{
    call_count++;
}

int main()
{
    struct spi_ctx spi0;
    struct spi_request req;

    uint8_t txbuf[BUFSZ];
    uint8_t rxbuf[BUFSZ];
    uint8_t tmpbuf[BUFSZ];

    DDRK = 0xff;
    sei();

    uint8_t mosi = 2;
    uint8_t sck = 1;
    uint8_t ss = 0;

    spi_master_init(&spi0,
        &SPCR, SPI_TRANSMIT, SPI_SPEED4,
        &DDRB, mosi, sck, ss
    );

    req.txbuf = txbuf;
    req.rxbuf = rxbuf;
    req.sz = BUFSZ;
    req.handler_done = handler_done;
    
    // start test
    PORTK = 1;

    size_t count = 4000;
    for (size_t i = 0; i < count; i++) {
        fill_buf(txbuf, BUFSZ);
        memcpy(tmpbuf, txbuf, BUFSZ);
        memset(rxbuf, 0, BUFSZ);
        spi_transfer(&spi0, &req);

        /* wait for transfer complete */ 
        while (spi_isbusy(&spi0))
            {}

        if (memcmp(txbuf, tmpbuf, BUFSZ) != 0)
            error();
        
        if (memcmp(txbuf, rxbuf, BUFSZ) != 0)
            error();
    }

    if (call_count != count)
        error();

    // test is pass
    success();

    return 0;
}
