#include "ringbuf.h"

#include <stdio.h>


#define BUFSZ 64

static uint8_t buf[BUFSZ];

int read_write_by_byte_test()
{
    size_t op_count = BUFSZ * 30000 + 1;
    
    struct rbuf_struct rbuf;
    rbuf_init(&rbuf, buf, BUFSZ);
    for (uint8_t data = 0; op_count > 0; op_count--) {
        size_t datasz = sizeof(data);
        if (rbuf_write(&rbuf, &data, datasz) != datasz)
            return 1;

        uint8_t readed_data;
        if (rbuf_read(&rbuf, &readed_data, sizeof(readed_data)) != datasz)
            return 1;
    
        if (readed_data != data)
            return 1;
        
        if (data == 255)
            data = 0;
        else
            data++;
    }
    return 0;
}

struct test_struct {
    int (*test)();
    char *msg;
};

static struct test_struct tests[] = {
    {read_write_by_byte_test, "write and read by byte"},
};


int main(int argc, char *argv[])
{
    size_t failed = 0;
    size_t count = sizeof(tests) / sizeof(*tests);
    for (size_t i = 0; i < count; i++) {
        struct test_struct *ptest = &tests[i];
        int res = ptest->test();
        char *status = (res) ? "FAIL" : "PASS";
        printf("%s %s\n", status, ptest->msg);
        if (res)
            failed++;
    }
    printf((failed == 0) ? "ALL TESTS PASSED\n" : "HAVE FAILED TESTS\n");
    return (failed == 0) ? 0 : 1;
}
