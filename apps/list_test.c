#include <avr/io.h>
#include <util/delay.h>
#include <inttypes.h>

#include "mpool.h"
#include "list.h"


struct user_struct {
    struct list_ctx list_info;
    size_t index;
};

void error()
{
    while (1) {
        PORTK = 2;
        _delay_ms(1000);
        PORTK = 0;
        _delay_ms(1000);
    }
}

void success()
{
    while (1) {
        PORTK = 4;
        _delay_ms(1000);
        PORTK = 0;
        _delay_ms(1000);
    }
}

#define ELEM_COUNT 10

int main()
{
    static struct user_struct memory[ELEM_COUNT];
    struct mpool_ctx mpool;
    struct list_ctx head;
    struct list_ctx *phead = &head;

    DDRK = 0xff;

    // start test
    PORTK = 1;
    _delay_ms(500);

    list_head_init(phead);
    size_t count = list_count(phead);
    if (count != 0)
        error();

    mpool_init(&mpool, &memory[0], sizeof(memory), sizeof(*memory));
    for (size_t i = 0; i < ELEM_COUNT; i++) {
        struct user_struct *p = (struct user_struct *) mpool_alloc(&mpool);
        if (!p)
            error();
        
        p->index = i;
        list_add_to_tail(phead, (struct list_ctx *) p);
        count = list_count(phead);
        if (count != i + 1)
            error();
    }

    size_t index = 0;
    for (struct list_ctx *p = head.next; p != phead; p = p->next, index++) {
        struct user_struct *data = (struct user_struct *) p;
        if (data->index != index)
            error();
    }

    if (index != ELEM_COUNT)
            error();

    while (head.next != phead) {
        struct list_ctx *p = head.next;
        list_delete(p);
        mpool_free(&mpool, p);
    }

    count = list_count(phead);
    if (count != 0)
        error();

    count = mpool_free_count(&mpool);
    if (count != ELEM_COUNT)
        error();

    success();

    return 0;
}
