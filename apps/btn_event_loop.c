#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "irq.h"
#include "timer16.h"
#include "btn.h"


#define TMR_NODE_COUNT 3


#include <util/delay.h>

static void error()
{
    while (1) {
        PORTK = 4;
        _delay_ms(1000);
        PORTK = 0;
        _delay_ms(1000);
    }
}


static struct timer16_ctx tmr;
void init_timer()
{
    static struct timer_node pool_memory[TMR_NODE_COUNT];

    timer16_init(&tmr, pool_memory, sizeof(pool_memory),
        TIMER16_CMPA0, TIMER16_PRSCLR_1024,
        &TCCR1A, &TCCR1B, &TIFR1, &TCNT1L, &OCR1AL, &TIMSK1
    );
}

static struct btn_ctx btn0;
void init_btn()
{
    btn_init(&btn0, &tmr, &EICRA, &EIMSK, &EIFR, 0, 0, EXT_INT0);
}

static void btn_handler(void *dev, int events)
{
    if (events == BTN_PRESSED) {
        PORTK = 1;
    } else if (events == BTN_HOLD_DOWN) {
        PORTK ^= (1 << 1);
    } else if (events == BTN_RELEASED) {
        PORTK = 0;
    } else {
        error();
    }
}

int main()
{
    DDRK = 0xff;

    init_timer();
    init_btn();

    dispatch_init();
    int events = BTN_PRESSED | BTN_RELEASED | BTN_HOLD_DOWN;
    dispatch_add(&btn0, btn_handler, events);
    dispatch_start();

    return 0;
}
