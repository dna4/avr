#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <inttypes.h>

#include "uart.h"
#include "ringbuf.h"
#include "irq.h"


#define BUFSZ 64
static struct uart_ctx uart0;

static void uart0_init()
{
    static uint8_t txbuf[BUFSZ];
    static uint8_t rxbuf[BUFSZ];

    uart_init(&uart0, &rxbuf[0], sizeof(rxbuf), &txbuf[0], sizeof(txbuf),
        USART_RX, USART_EMPTY, 9600, &UCSR0A);
}

void error()
{
    while (1) {
        PORTB = 4;
        _delay_ms(1000);
        PORTB = 0;
        _delay_ms(1000);
    }
}

int main()
{
    size_t count = BUFSZ - 1;
    DDRB = 0xff;
    sei();

    // start test
    PORTB = 1;

    uart0_init();

    if (uart_test_reg_addrs(&uart0, &UCSR0A, &UCSR0B, &UCSR0C, &UBRR0L, &UBRR0H, &UDR0))
        error();
    
    for (size_t i = 0; i < count; i++) {
        uint8_t data = i % 256;
        size_t len = sizeof(data);
        if (uart_write(&uart0, &data, len) != len)
            error();
    }

    for (size_t i = 0; i < count; i++) {
        uint8_t data;
        size_t len = sizeof(data);
        while (1) {
            size_t read_count = uart_read(&uart0, &data, len);
            if (read_count > 0)
                break;
        }
        uint8_t right_data = i % 256;
        if (data != right_data)
            error();
    }

    if (uart_have_errs(&uart0))
        error();

    uint8_t data;
    size_t read_count = uart_read(&uart0, &data, sizeof(data));
    if (read_count > 0)
        error();

    // success led blink
    PORTB = 2;
    while (1) {
        _delay_ms(1000);
        uint8_t flag = 0b00000010;
        if (PORTB & flag)
            PORTB &= ~flag;
        else
            PORTB |= flag;
    }

    return 0;
}
