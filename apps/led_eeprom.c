#include <avr/io.h>
#include <util/delay.h>
#include "eepromio.h"

int main()
{
    DDRD = 0xFF;
    uint8_t leds = 0;
    void *addr = 0;
    while(1) {
        leds = (leds > 0) ? leds << 1 : 1;
        eeprom_write(addr, leds);
        leds = 0xff;
        _delay_ms(1000);
        leds = eeprom_read(addr);
        PORTD = leds;
    }
    return 0;
}
