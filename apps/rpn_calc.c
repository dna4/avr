#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <inttypes.h>
#include <ctype.h>
#include <string.h>

#include "libio.h"
#include "istack.h"

struct token {
    int type;
    int32_t val;
    int op;
};

#define STRLEN 12

enum {
    NUMBER = 0,
    OPERATION,
};

enum {
    ADD = 0,
    SUB,
    MUL,
    DIV,
};

struct op_info {
    int type;
    char ch;
};

#define STKBUFSZ 24

static int32_t stkbuf[STKBUFSZ];
static struct istack stk;

int to_op(char *str, int *op)
{
    static struct op_info ops[] = {
        {ADD, '+'},
        {SUB, '-'},
        {MUL, '*'},
        {DIV, '/'},
    };

    size_t count = sizeof(ops) / sizeof(ops[0]);
    for (size_t i = 0; i < count; i++)
        if (ops[i].ch == str[0] && str[1] == '\0') {
            *op = ops[i].type;
            return 0;
        }
    return 1;
}

int to_int32(char *str, int32_t *val)
{
    int is_neg = *str == '-';
    
    if (is_neg)
        str++; // skip sign

    if (!isdigit(*str))
        return 1;

    int32_t v = 0;
    for (char ch; (ch = *str) != '\0'; str++) {
        if (!isdigit(ch))
            return 1;

        v = (v * 10) + (ch - '0');
    }

    if (is_neg)
        v *= -1;

    *val = v;
    
    return 0;
}

int int32_to_str(char *str, size_t sz, int32_t v)
{
    if (sz < 2)
        return 1;

    if (v < 0) {
        *str = '-';
        str++;
        v *= -1;
        sz--;
    }

    size_t i = 0;
    do {
        if (i + 1 >= sz)
            return 1;

        int32_t rest = v % 10;
        str[i] = rest + '0';
        i++;
        v /= 10;
    } while (v);

    str[i] = '\0';
    
    i--;
    for (size_t j = 0; j < i; j++, i--) {
        char tmp = str[i];
        str[i] = str[j];
        str[j] = tmp;
    }

    return 0;
}

void get_token(struct token *tkn)
{
    char str[STRLEN];

    for (;;) {
        if (get_string(str, STRLEN)) {
            print("Error: received too long string\n");
            skip_string();
            continue;
        }

        if (to_op(str, &tkn->op) == 0) {
            tkn->type = OPERATION;
            break;
        } else if (to_int32(str, &tkn->val) == 0) {
            tkn->type = NUMBER;
            break;
        }

        print("Error: received string is not number or op\n");
    }
}

void handle_token(struct token *tkn)
{
    int type = tkn->type;
    if (type == NUMBER) {
        if (istack_push(&stk, tkn->val))
            print("Error: can't push value to stack\n");
    } else if (type == OPERATION) {
        int32_t lval, rval;

        if (istack_pop(&stk, &rval)) {
            print("Error: can't pop second operand from stack\n");
            return;
        }

        if (istack_pop(&stk, &lval)) {
            print("Error: can't pop first operand from stack\n");
            return;
        }

        int32_t res;
        int op = tkn->op;
        if (op == ADD) {
            res = lval + rval;
        } else if (op == SUB) {
            res = lval - rval;
        } else if (op == MUL) {
            res = lval * rval;
        } else if (op == DIV) {
            res = lval / rval;
        } else {
            return;
        }

        istack_push(&stk, res);
        char resstr[STRLEN + 1]; // one byte for new line char
        if (int32_to_str(resstr, STRLEN, res)) {
            print("Error: can't convert int32 to string\n");
            return;
        }

        size_t end = strlen(resstr);
        resstr[end] = '\n';
        resstr[end + 1] = '\0';
        print(resstr);
    }
}

int main()
{
    io_init();
    istack_init(&stk, &stkbuf[0], sizeof(stkbuf));

    sei();
    for (;;) {
        struct token tkn;

        get_token(&tkn);
        handle_token(&tkn);
    }

    return 0;
}
