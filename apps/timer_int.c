#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <inttypes.h>

void timer_init()
{
    TCCR0B = 0; // stop timer
    TIFR0 = 1; // clear overflow bit
    TCNT0 = 0; // set timer count
    TCCR0B = 1; // start timer normal
    TIMSK0 = 1 << TOIE0;
}

uint32_t count = 0;
ISR (TIMER0_OVF_vect)
{
    if (count == 300000) {
        if (PORTD & 2)
            PORTD -= 2;
        else
            PORTD += 2;
        count = 0;
    } else {
        count++;
    }
}

void toggle()
{
    if (PORTD & 1)
        PORTD -= 1;
    else
        PORTD += 1;
}

int main()
{
    DDRD = 0xff;
    sei();
    timer_init();
    while(1) {
        toggle();
        _delay_ms(1000);
        toggle();
        _delay_ms(1000);
    }
    return 0;
}
