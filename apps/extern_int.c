#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <inttypes.h>

void int0_init()
{
    EICRA = 2;
    EIMSK = 1 << INT0;
}

uint32_t is_press = 0;
ISR (INT0_vect)
{
    if (is_press == 0) {
        is_press = 1;
    } else {
        is_press = 0;
    }
}

int main()
{
    DDRD = 0b00000001;
    int0_init();
    sei();
    while(1) {
        if (is_press) {
            PORTD = 1;
        } else {
            PORTD = 0;
        }
    }
    return 0;
}
