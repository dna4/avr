#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <inttypes.h>

#include "uart.h"
#include "ringbuf.h"
#include "irq.h"


#define BUFSZ 16


int main()
{
    static uint8_t txbuf[BUFSZ];
    static uint8_t rxbuf[BUFSZ];
    struct uart_ctx uart0;

    uart_init(&uart0, &rxbuf[0], sizeof(rxbuf), &txbuf[0], sizeof(txbuf),
        USART_RX, USART_EMPTY, 9600, &UCSR0A);

    sei();

    for (;;) {
        uint8_t data;

        while (uart_read(&uart0, &data, 1) != 1)
            {}

        while (uart_write(&uart0, &data, 1) != 1)
            {}
    }
    return 0;
}
