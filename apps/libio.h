#include <inttypes.h>

#include "uart.h"
#include "ringbuf.h"
#include "irq.h"


void io_init();
void print(char *str);
char get_char();
int get_string(char *str, size_t sz);
void skip_string();
