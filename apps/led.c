#include <avr/io.h>
#include <util/delay.h>

int main()
{
    DDRD = 0xFF;

    while(1) {
        PORTD = ~PORTD;
        _delay_ms(500);
    }
    return 0;
}
