#include <avr/interrupt.h>
#include <stdlib.h>
#include "irq.h"


struct irq_desc {
    handler_fn handler;
    void *arg;
};

static struct irq_desc irq_array[] = {
    {NULL, NULL},
    {NULL, NULL},
};


static int check_irq(uint8_t irq)
{
    uint8_t size = sizeof(irq_array) / sizeof(*irq_array);
    return irq >= size;
}

void irq_request(uint8_t irq, handler_fn handler, void *arg)
{
    if (check_irq(irq) || !handler)
        return;

    struct irq_desc *desc = &irq_array[irq];
    desc->handler = handler;
    desc->arg = arg;
}

void irq_free(uint8_t irq)
{
    if (check_irq(irq))
        return;

    struct irq_desc *desc = &irq_array[irq];
    desc->handler = NULL;
    desc->arg = NULL;
}

static void irq_handle(uint8_t irq)
{
    struct irq_desc *desc = &irq_array[irq];
    desc->handler(desc->arg);
}

ISR(USART_RX_vect)
{
    irq_handle(USART_RX);
}

ISR(USART_UDRE_vect)
{
    irq_handle(USART_EMPTY);
}
