#ifndef IRQ_GUARD
#define IRQ_GUARD


#define IRQ_SAFE_SLEEP_MCU

#define Q(x) #x
#define QUOTE(x) Q(x)


#include QUOTE(MCU)
#include <inttypes.h>

typedef void (*handler_fn)(void *arg);


void irq_request(uint8_t irq, handler_fn handler, void *arg);
void irq_free(uint8_t irq);

#ifdef IRQ_SAFE_SLEEP_MCU
void irq_sleep_mcu();
void irq_reset_sleep_flag();
#endif

#endif
