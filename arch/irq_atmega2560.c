#include <avr/interrupt.h>
#include <stdlib.h>

#include "irq.h"

#ifdef IRQ_SAFE_SLEEP_MCU
#include <avr/sleep.h>
#endif


struct irq_desc {
    handler_fn handler;
    void *arg;
};

static struct irq_desc irq_array[] = {
    {NULL, NULL},
    {NULL, NULL},
    {NULL, NULL},
    {NULL, NULL},
    {NULL, NULL},
    {NULL, NULL},
};

#ifdef IRQ_SAFE_SLEEP_MCU
static volatile uint8_t have_new_event = 0;
#endif

static int check_irq(uint8_t irq)
{
    uint8_t size = sizeof(irq_array) / sizeof(*irq_array);
    return irq >= size;
}

void irq_request(uint8_t irq, handler_fn handler, void *arg)
{
    if (check_irq(irq) || !handler)
        return;

    struct irq_desc *desc = &irq_array[irq];
    desc->handler = handler;
    desc->arg = arg;
}

void irq_free(uint8_t irq)
{
    if (check_irq(irq))
        return;

    struct irq_desc *desc = &irq_array[irq];
    desc->handler = NULL;
    desc->arg = NULL;
}

#ifdef IRQ_SAFE_SLEEP_MCU
void irq_reset_sleep_flag()
{
    have_new_event = 0;
}

void irq_sleep_mcu()
{
    cli();
    if (!have_new_event) {
        sleep_enable();
        sei();
        sleep_cpu();
        sleep_disable();
    }
    sei();
}
#endif

static void irq_handle(uint8_t irq)
{
    struct irq_desc *desc = &irq_array[irq];
    desc->handler(desc->arg);

#ifdef IRQ_SAFE_SLEEP_MCU
    have_new_event = 1;
#endif
}

ISR(USART0_RX_vect)
{
    irq_handle(USART_RX);
}

ISR(USART0_UDRE_vect)
{
    irq_handle(USART_EMPTY);
}

ISR(TIMER0_COMPA_vect)
{
    irq_handle(TIMER8_CMPA0);
}

ISR(TIMER1_COMPA_vect)
{
    irq_handle(TIMER16_CMPA0);
}

ISR(SPI_STC_vect)
{
    irq_handle(SPI_TRANSMIT);
}

ISR(INT0_vect)
{
    irq_handle(EXT_INT0);
}
